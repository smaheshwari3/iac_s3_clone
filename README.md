# iac-s3

Project to deploy private encrypted s3 buckets

## Summary

This repository consolidates IaC s3 buckets under version control as a monorepo.

All pet servers are deployed using an EC2 Terraform module located at https://gitlab.mgmt.core.mcs.bravurasolutions.net/bsl-global-aws-project/terraform-aws-s3

## Deployment
Deploying servers is done with the following process.

1. `SUBFOLDER=bucket-folder make plan` e.g. `SUBFOLDER=bslbucket-iac-s3 make plan`
2. Review the plan output. If the results of the plan are satisfactory proceed to step 3.
3. `SUBFOLDER=bucket-folder make apply` e.g. `SUBFOLDER=bslbucket-iac-s3 make apply`
