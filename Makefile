ifdef CI
	PROFILE_REQUIRED=profile
	INIT_REQUIRED=init workspace
	TF_IN_AUTOMATION=true
	GITCONFIG_REQUIRED=gitconfig
	# In CI use the docker volume instead of the host aws config path
	AWS_MOUNT=config:/root
endif

ifdef TERRAFORM_BACKEND_PROFILE
	BACKEND_CONFIG := ${BACKEND_CONFIG} -backend-config="profile=${TERRAFORM_BACKEND_PROFILE}" -input=false -backend=true
endif

ifdef TERRAFORM_BACKEND_ROLE_ARN
	BACKEND_CONFIG := ${BACKEND_CONFIG} -backend-config="role_arn=${TERRAFORM_BACKEND_ROLE_ARN}" -input=false -backend=true
endif

export SUBFOLDER ?= .
export CI_PROJECT_NAME ?= docker-volume
export CI_JOB_ID ?= local
export AWS_MOUNT ?= ~/.aws:/root/.aws


init: .env $(PROFILE_REQUIRED) $(GITCONFIG_REQUIRED)
	docker-compose run --rm terraform-utils terraform init $(BACKEND_CONFIG)
.PHONY: init

plan: $(INIT_REQUIRED)
	docker-compose run --rm terraform-utils terraform plan
.PHONY: plan

apply: $(INIT_REQUIRED)
	docker-compose run --rm terraform-utils terraform apply
.PHONY: apply

applyAuto: $(INIT_REQUIRED)
	docker-compose run --rm terraform-utils terraform apply -auto-approve
.PHONY: applyAuto

output: $(INIT_REQUIRED)
	docker-compose run --rm terraform-utils sh -c 'terraform output -no-color > terraform.tfvars'
.PHONY: output

outputJson: $(INIT_REQUIRED)
	docker-compose run --rm terraform-utils sh -c 'terraform output -json -no-color > output.json'
.PHONY: output

destroy: $(INIT_REQUIRED)
	docker-compose run --rm terraform-utils terraform destroy
.PHONY: destroy

destroyAuto: $(INIT_REQUIRED)
	docker-compose run --rm terraform-utils terraform destroy -auto-approve
.PHONY: destroyAuto

workspace: .env $(PROFILE_REQUIRED)
	docker-compose run --rm envvars ensure
	docker-compose run --rm terraform-utils sh -c 'terraform workspace new $${TERRAFORM_WORKSPACE} || terraform workspace select $${TERRAFORM_WORKSPACE}'
.PHONY: workspace

validate: $(INIT_REQUIRED)
	docker-compose run --rm envvars ensure
	docker-compose run --rm terraform-utils terraform fmt -diff -check -recursive
	docker-compose run --rm terraform-utils terraform validate
.PHONY: validate

version: .env
	docker-compose run --rm terraform-utils terraform version
.PHONY: version

check: .env
	docker-compose run --rm terraform-utils terraform fmt -check -diff -recursive
.PHONY: check

fmt: .env
	docker-compose run --rm terraform-utils terraform fmt -diff -recursive
.PHONY: fmt

shell: .env
	docker-compose run --rm terraform-utils sh
.PHONY: shell

profile: .env
	# Update target account profile
	docker-compose run --rm aws env -u AWS_PROFILE aws configure set credential_source Ec2InstanceMetadata --profile ${AWS_PROFILE}
	docker-compose run --rm aws aws configure set role_arn arn:aws:iam::${AWS_ACCOUNT_ID}:role/${AWS_ROLE_NAME} --profile ${AWS_PROFILE}
	@docker-compose run --rm aws aws configure set external_id ${GITLAB_EXTERNAL_ID} --profile ${AWS_PROFILE}
.PHONY: profile

.env:
	touch .env
	docker-compose run --rm envvars validate
	docker-compose run --rm envvars envfile --overwrite
.PHONY: .env

import: $(INIT_REQUIRED) 
	docker-compose run --rm envvars ensure
	docker-compose run --rm terraform-utils terraform import ${IMPORT_RESOURCE_NAME} ${IMPORT_RESOURCE_ID}
.PHONY: import

gitconfig: 
	# Required git configuration for Terraform using private repos and tokens. Using gitlab-ci-token and CI_JOB credentials
	@docker-compose run --rm terraform-utils sh -c 'git config --replace-all --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com".insteadOf https://gitlab.com'
.PHONY: gitconfig


