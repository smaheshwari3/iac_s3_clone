terraform {
  backend "s3" {
    dynamodb_table       = "bravura-iac-terraform-lock"
    bucket               = "bravura-iac-terraform-backend"
    workspace_key_prefix = "tf-state"
    key                  = "bravura-iac-loki-storage"
    region               = "ap-southeast-2"
    encrypt              = true
  }
}
