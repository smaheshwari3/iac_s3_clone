terraform {
  backend "s3" {
    dynamodb_table       = "bravura-iac-terraform-lock"
    bucket               = "bravura-iac-terraform-backend"
    workspace_key_prefix = "tf-state"
    key                  = "bravura-iac-bslbucket-iac-s3"
    region               = "ap-southeast-2"
    encrypt              = true
  }
}
