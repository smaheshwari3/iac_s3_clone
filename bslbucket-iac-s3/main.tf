resource "aws_kms_key" "objects" {
  description             = "KMS key is used to encrypt bucket objects"
  deletion_window_in_days = local.workspace.delete_window_days
}

resource "aws_iam_role" "s3_bucket_role" {
  name                  = lower("${local.workspace.bucket_name}-s3-role")
  description           = "Role used by ec2 for s3 access"
  force_detach_policies = local.workspace.force_detach_policies
  assume_role_policy    = data.aws_iam_policy_document.ec2_assume_policy.json

  tags = local.common_tags
}

module "s3_bucket" {
  source        = "git::https://gitlab.com/bravura/shared/terraform/aws/terraform-aws-s3.git?ref=0.1.2"
  bucket        = local.workspace.bucket_name
  acl           = local.workspace.bucket_acl
  force_destroy = local.workspace.force_destroy

  attach_policy = local.workspace.attach_policy
  policy        = data.aws_iam_policy_document.bucket_policy.json

  tags = local.common_tags

  versioning = {
    enabled = local.workspace.bucket_versioning
  }

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = aws_kms_key.objects.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  # S3 bucket-level Public Access Block configuration
  block_public_acls       = local.workspace.block_public_acls
  block_public_policy     = local.workspace.block_public_policy
  ignore_public_acls      = local.workspace.ignore_public_acls
  restrict_public_buckets = local.workspace.restrict_public_buckets
}
