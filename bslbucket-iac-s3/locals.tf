locals {
  workspace = local.env[terraform.workspace]
  env = {
    nonprod = {
      aws_region = "ap-southeast-2"
      # s3
      bucket_name             = "bslbucket-iac-s3"
      bucket_acl              = "private"
      bucket_versioning       = false
      delete_window_days      = 7
      force_detach_policies   = true
      force_destroy           = true
      attach_policy           = true
      block_public_acls       = true
      block_public_policy     = true
      ignore_public_acls      = true
      restrict_public_buckets = true
    }
  }
}
locals {
  common_tags = {
    "managed_by"  = "terraform"
    "environment" = terraform.workspace,
  }
}
